import React, { useEffect, useState } from "react";

const BMI = () => {
  const [height, setHeight] = useState(0);
  const [weight, setWeight] = useState(0);
  const [bmi, setBMI] = useState(0);

  const handleHeightChange = (e) => {
    setHeight(e.target.value);
  };

  const handleWeightChange = (e) => {
    setWeight(e.target.value);
  };

  // Update BMI when height or weight change
  useEffect(() => {
    // Calculate BMI using the correct formula: weight (kg) / (height (m))^2
    const heightInMeters = height / 100;
    const bmiValue = weight / heightInMeters ** 2;
    setBMI(bmiValue.toFixed(2));
  }, [height, weight]);

  return (
    <div className="mt-4 d-flex align-items-center justify-content-center">
      <div
        className="card form-card border-color custom-bg"
        style={{ width: "25rem" }}
      >
        <div className="card-header text-center custom-bg">
          <h4 className="card-title text-color-1">BMI CALCULATOR</h4>
        </div>
        <div className="card-body text-color-1">
          <form>
            <div className="mb-3">
              <label htmlFor="height" className="form-label">
                <b>Height (cm)</b>
              </label>
              <input
                type="number"
                className="form-control"
                id="height"
                name="height"
                placeholder="Enter Height..."
                onChange={handleHeightChange}
                value={height}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="weight" className="form-label">
                <b>Weight (kg)</b>
              </label>
              <input
                type="number"
                className="form-control"
                id="weight"
                name="weight"
                placeholder="Enter Weight..."
                onChange={handleWeightChange}
                value={weight}
              />
            </div>
          </form>

          <div className="text-center">
            <h2>BMI : {isNaN(bmi) ? 0 : bmi}</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BMI;
