const ContactUs = () => {
  return (
    <div className="container mt-3">
      <h1 className="text-color-1">Contact Us</h1>
      <h2 className="text-color-1 mt-3">Get in Touch with Our Friendly Team</h2>
      <p className="text-color-1">
        At The Gym, we're always here to help you on your fitness journey.
        Whether you have questions, suggestions, or simply want to share your
        experience, we'd love to hear from you. Please feel free to reach out
        using any of the methods below, and our friendly staff will get back to
        you as soon as possible.
      </p>

      <div className="row">
        <div className="col-lg-6 col-md-12">
          <h3 className="text-color-1 mt-3">Phone</h3>
          <p className="text-color-1">
            Talk to one of our knowledgeable team members by giving us a call
            during our regular business hours. We're always happy to help!
          </p>
          <p className="text-color-1">
            Phone Number: (123) 456-7890 <br />
            Business Hours: Monday - Friday, 6:00 AM - 10:00 PM | Saturday -
            Sunday, 8:00 AM - 6:00 PM
          </p>
        </div>

        <div className="col-lg-6 col-md-12">
          <h3 className="text-color-1 mt-3">Email</h3>
          <p className="text-color-1">
            Prefer to send us an email? Drop us a line at the address below, and
            we'll do our best to respond within 24 hours.
          </p>
          <p className="text-color-1">Email: support@thegym.com</p>
        </div>
      </div>

      <h3 className="text-color-1 mt-3">Social Media</h3>
      <p className="text-color-1">
        Stay connected and join our online community! We frequently post
        updates, special offers, and fitness tips. Feel free to send us a
        message or comment on our posts.
      </p>
      <p className="text-color-1">
        Facebook: www.facebook.com/thegym <br />
        Instagram: @thegym <br />
        Twitter: @thegym
      </p>

      <h3 className="text-color-1 mt-3">Visit Us</h3>
      <p className="text-color-1">
        If you're in the neighborhood, feel free to stop by our facility during
        business hours. Our staff will be more than happy to give you a tour,
        answer any questions, or help you sign up for a membership.
      </p>
      <p className="text-color-1">
        Address: 123 Fitness Street, GymCity, ST, 12345
      </p>

      <p className="mt-5, text-color-1">
        Thank you for your interest in The Gym. We look forward to helping you
        achieve your fitness goals and become the best version of yourself.
      </p>
    </div>
  );
};

export default ContactUs;
