import { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";

const AdminLoginForm = () => {
  let navigate = useNavigate();

  const [loginRequest, setLoginRequest] = useState({
    username: "",
    password: "",
  });

  const handleUserInput = (e) => {
    setLoginRequest({ ...loginRequest, [e.target.name]: e.target.value });
  };

  const loginAction = (e) => {
    const formData = new FormData();
    formData.append("username", loginRequest.username);
    formData.append("password", loginRequest.password);

    fetch("http://localhost:8080/api/admin/login", {
      method: "POST",
      body: formData,
      mode: "cors",
    }).then((result) => {
      result
        .json()
        .then((res) => {
          sessionStorage.setItem("active-user", "Admin");
          toast.success("Successfully Logged In", {
            position: "top-center",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          navigate("/");
          window.location.reload(true);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
          toast.error("Error Logging In", {
            position: "top-center",
            autoClose: 1000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        });
    });
    e.preventDefault();
  };

  return (
    <div className="mt-4 d-flex aligns-items-center justify-content-center">
      <div
        className="card form-card border-color custom-bg"
        style={{ width: "25rem" }}
      >
        <div className="card-header text-center text-color-1">
          <h4 className="card-title">Admin Login</h4>
        </div>
        <div className="card-body text-color-1">
          <form>
            <div className="mb-3 text-color">
              <label for="username" class="form-label">
                <b>Username</b>
              </label>
              <input
                //type="email"
                className="form-control"
                id="username"
                name="username"
                onChange={handleUserInput}
                value={loginRequest.username}
              />
            </div>
            <div className="mb-3 text-color">
              <label for="password" className="form-label">
                <b>Password</b>
              </label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                onChange={handleUserInput}
                value={loginRequest.password}
                autoComplete="on"
              />
            </div>
            <div className="d-flex justify-content-center">
              <button
                type="submit"
                className="btn custom-bg-1 text-color"
                onClick={loginAction}
              >
                Login
              </button>
            </div>
            <ToastContainer />
          </form>
        </div>
      </div>
    </div>
  );
};

export default AdminLoginForm;
