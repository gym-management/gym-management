import Footer from "../components/Footer";
import Welcome from "../components/Welcome";

const HomePage = () => {
  return (
    <div className="container-fluid mb-2">
      <Welcome />
      <hr />
      <Footer />
    </div>
  );
};

export default HomePage;
