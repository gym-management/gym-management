import React from "react";

const AboutUs = () => {
  return (
    <div className="container mt-3">
      <h1 className="text-color-1">About Us</h1>
      <h2 className="text-color-1 mt-3">Welcome to The Gym</h2>
      <p className="text-color-1">
        The Gym was founded with a mission to provide a welcoming, supportive,
        and results-driven environment for individuals at all stages of their
        fitness journeys. Our state-of-the-art facility, experienced trainers,
        and wide range of classes ensure that everyone can find a path to
        achieving their health and wellness goals.
      </p>

      <div className="row">
        <div className="col-lg-6 col-md-12">
          <h3 className="text-color-1 mt-3">Our Facility</h3>
          <p className="text-color-1">
            Our spacious gym spans over 20,000 square feet and features a wide
            variety of top-of-the-line equipment, including cardio machines,
            strength training equipment, and functional training zones. Our
            members can also enjoy access to comfortable locker rooms, showers,
            and a dedicated stretching area.
          </p>
        </div>

        <div className="col-lg-6 col-md-12">
          <h3 className="text-color-1  mt-3">Experienced Trainers</h3>
          <p className="text-color-1">
            Our team of certified fitness professionals are passionate about
            helping members reach their fitness goals. With diverse backgrounds
            and specializations, our trainers can provide personalized guidance,
            motivation, and support to help you achieve the results you desire.
          </p>
        </div>
      </div>

      <h3 className="text-color-1 mt-3">Wide Range of Classes</h3>
      <p className="text-color-1">
        At The Gym, we offer a variety of group fitness classes to suit all
        interests and fitness levels. From high-intensity interval training and
        strength training to yoga and Pilates, our classes are led by
        experienced instructors who create fun, engaging, and challenging
        workouts.
      </p>

      <h3 className="text-color-1 mt-3">Join Our Community</h3>
      <p className="text-color-1">
        We believe that fitness is more than just a workout; it's a lifestyle.
        That's why we strive to create a friendly, supportive community where
        members can connect, share their experiences, and encourage one another
        along the way. Join us at The Gym and become part of our growing family
        of fitness enthusiasts!
      </p>
    </div>
  );
};

export default AboutUs;
