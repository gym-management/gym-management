import { Routes, Route } from "react-router-dom";
import ContactPage from "../screens/ContactPage";
import AboutPage from "../screens/AboutPage";
import HomePage from "../screens/HomePage";
import BMI from "../screens/BMI";
import CustomerLoginForm from "../screens/CustomerLoginForm";
import AdminLoginForm from "../screens/AdminLoginForm";
import RegisterAdminForm from "../screens/RegisterAdminForm";
import RegisterCustomerForm from "../screens/RegisterCustomerForm";
import AddTrainerForm from "../screens/RegisterTrainerForm";
import AllTrainers from "../screens/AllTrainers";
import SearchCustomer from "../screens/SearchCustomer";
import AddMembership from "../screens/AddMembership";
import AddPackageForm from "../screens/AddPackageForm";
import AllPackage from "../screens/AllPackage";
import CustomerMembership from "../screens/CustomerMembership";
import MyMembership from "../screens/MyMembership";
import CustomerProfile from "../screens/CustomerProfile";
import CustomerChangePassword from "../screens/CustomerChangePassword";
import CustomerForgetPassword from "../screens/CustomerForgetPassword";
import UpdateCustomerDetails from "../screens/UpdateCustomerDetails";
import UpdateCustomerProfile from "../screens/UpdateCustomerProfile";

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="about" element={<AboutPage />} />
      <Route path="contact" element={<ContactPage />} />
      <Route path="/bmi/calculator" element={<BMI />} />
      <Route path="customer/login" element={<CustomerLoginForm />} />
      <Route path="admin/login" element={<AdminLoginForm />} />
      <Route path="admin/register" element={<RegisterAdminForm />} />
      <Route path="customer/register" element={<RegisterCustomerForm />} />
      <Route path="trainer/register" element={<AddTrainerForm />} />
      <Route path="trainer/all" element={<AllTrainers />} />
      <Route path="admin/customer/search" element={<SearchCustomer />} />
      <Route path="admin/membership/add" element={<AddMembership />} />
      <Route path="package/add" element={<AddPackageForm />} />
      <Route path="package/all" element={<AllPackage />} />
      <Route path="customer/membership/" element={<MyMembership />} />
      <Route path="customer/profile/" element={<CustomerProfile />} />
      <Route
        path="/admin/customer/membership/:clientId"
        element={<CustomerMembership />}
      />
      <Route
        path="customer/changepassword/"
        element={<CustomerChangePassword />}
      />
      <Route
        path="customer/forgetpassword/"
        element={<CustomerForgetPassword />}
      />
      <Route
        path="customer/profile/updatedetails"
        element={<UpdateCustomerDetails />}
      />
      <Route
        path="customer/profile/update"
        element={<UpdateCustomerProfile />}
      />
    </Routes>
  );
}

export default App;
