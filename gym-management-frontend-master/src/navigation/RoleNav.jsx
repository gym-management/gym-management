import HeaderCustomer from "../components/HeaderCustomer";
import HeaderAdmin from "../components/HeaderAdmin";
import HeaderDefault from "../components/HeaderDefault";

const RoleNav = () => {
  const user = sessionStorage.getItem("active-user");

  if (user === "Admin") return <HeaderAdmin />;
  else if (user === "Customer") return <HeaderCustomer />;
  else return <HeaderDefault />;
};

export default RoleNav;
