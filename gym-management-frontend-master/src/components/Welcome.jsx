const Welcome = () => {
  return (
    <div className="text-center mt-3">
      <div className="text-color-1">
        MORE THAN JUST A GYM
        <h3 className="mt-2">WELCOME TO THE GYM</h3>
      </div>

      <div className="col-md-8 offset-md-2 text-color-1">
        Being a 24/7 gym is one of our best amenities because we can offer our
        members unrestricted freedom of access from sun up to sun down and
        everywhere in between. You no longer have to worry about cramming in gym
        time before we close!
      </div>
    </div>
  );
};

export default Welcome;
