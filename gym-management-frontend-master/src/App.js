import Header from "./components/Header";
import Routes from "./navigation/Routes";

function App() {
  return (
    <>
      <Header />
      <Routes />
    </>
  );
}

export default App;
