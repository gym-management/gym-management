GYM management Web Application
Application consists of React frontend and spring boot backend.
gym-management-frontend-master is front end service Gateway, Adminservice,Customerservice,Membershipservice,Packageservice,Trainerservice are the backend microservice.
Admin and user registers themselfs into the network.
In order to start the program, the following commands must be run:

cd ./gym-management-frontend-master
(/gym-management-frontend-master) npm install react --save
cd ../
docker-compose build
docker-compose up

cd ./Gateway
cd ../
docker build
docker up

cd ./Adminservice
cd ../
docker-compose build
docker-compose up

cd ./Customerservice
cd ../
docker-compose build
docker-compose up

cd ./Membershipservice
cd ../
docker-compose build
docker-compose up

cd ./Packageservice
cd ../
docker-compose build
docker-compose up

cd ./Trainerservice
cd ../
docker-compose build
docker-compose up
